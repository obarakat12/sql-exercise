1. Get all the names of students in the database

SELECT Name FROM students

"Alex"
"Basma"
"Hasan"
"Jana"
"Layal"
"Robert"

2. Get all the data of students above 30 years old

SELECT * FROM students WHERE Age > 30

"5"	"Robert"	"34"	"M"	"500"
"6"	"Jana"	"33"	"F"	"500"

3. Get the names of the females who are 30 years old

SELECT Name FROM students WHERE Age=30 and Gender='F'

0 rows returned

4. Get the number of Points of Alex

SELECT Points FROM students WHERE Name = 'Alex'

200

5. Add yourself as a new student (your name, your age...)

INSERT INTO students (Name, Age, Gender, Points) VALUES ('Omar', '30', 'M', '999')

Query executed successfully, 1 rows affected.

6. Increase the points of Basma because she solved a new exercise

update students set points = Points+100 WHERE Name='Basma'

Query executed successfully, 1 rows affected.

7. Decrease the points of Alex because he's late today

update students set points = Points-10 WHERE Name='Alex'

Query executed successfully, 1 rows affected.

8. Copy Layal's data from students to graduates

insert INTO graduates (Name, Age, Gender, Points)
SELECT Name, Age, Gender, Points FROM students WHERE Name = 'Layal'

9. Add the graduation date previously mentioned to Layal's record in graduates

update graduates set graduation = '08/09/2018'
where Name = 'Layal'

10. Remove Layal's record from students

DELETE FROM students
WHERE Name = 'Layal'

11. Produce a table that contains, for each employee, his/her name, company name, and company date.

SELECT employees.Name, companies.Name, companies.Date 
FROM employees join companies
ON companies.Name = employees.Company

"Marc"	"Google"	"1998"
"Maria"	"Google"	"1998"
"Alaa"	"Facebook"	"2004"
"Hala"	"Snapchat"	"2011"

12. Find the name of employees that work in companies made before 2000.

SELECT employees.Name
FROM employees join companies
ON companies.Name = employees.Company
WHERE companies.Date < 2000

"Marc"
"Maria"

13. Find the name of company that has a graphic designer.

SELECT companies.Name
FROM companies join employees
ON companies.Name = employees.Company
AND employees.Role = 'Graphic Designer'

Snapchat

14. Find the person with the highest number of points in students

SELECT Name FROM students 
WHERE Points = (SELECT MAX(Points) FROM students)

15. Find the average of points in students

SELECT AVG(Points) AS Average FROM students

16. Find the number of students that have 500 points

SELECT COUNT(*) FROM students WHERE Points = 500

17. Find the names of students that contains 's'

SELECT Name FROM students WHERE Name LIKE '%s%'

18. Find all students based on the decreasing order of their points

SELECT Name FROM students ORDER BY Points DESC
